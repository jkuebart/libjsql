libjsql
=======


Building
--------

`libjsql` requires the [sqlite][SQLITE] library to be present. It is built
into the `build/` directory using [Scons][SCONS]:

    $ scons

The debug version is built like this:

    $ scons MODE=debug

This also creates a `compile_commands.json` database used by some LLVM
tools.

In order to check the source code using [include-what-you-use][IWYU] on
Darwin, use

    $ iwyu_tool.py -p . -- -isysroot "$(xcrun --show-sdk-path)"


Usage
-----

This example uses the library to create a database, store a row and read it
again.

In order to use the library, the relevant headers need to be included.

```cpp
#include "libjsql/Db.h"
#include "libjsql/Query.h"
#include "libjsql/SimpleQuery.h"
#include "libjsql/Statement.h"

#include <iostream>
```

The remaining code becomes less verbose by `using` the required
identifiers.

```cpp
using jsql::Db;
using jsql::Query;
using jsql::SimpleQuery;
using jsql::Statement;
```

This function opens a database file for writing and creates it if it does
not exist. Then it creates a new table if it does not exist and inserts two
rows into the table. The function uses the class `SimpleQuery` because the
SQL statements it executes have no parameters, so using a prepared
statement has no advantage. At the end of the function, the database file
is closed automatically.
```cpp
namespace
{
void writeDb(char const* const filename)
{
    Db const db{filename, Db::OPEN::CREATE | Db::OPEN::READWRITE};
    SimpleQuery{db, R"(
        create table if not exists "readme"("id" int, "name" text);
    )"};
    SimpleQuery{db, R"(
        insert into "readme" values (1, 'one'), (2, 'two');
    )"};
}
```

This function opens a database for reading.
```cpp
void readDb(char const* const filename)
{
    Db const db{filename};
```

It prepares a `SELECT` statement with a single parameter `id`.
```cpp
    Statement select_id{db, R"(
        select * from "readme" where "id" = ?;
    )"};
```

Finally, a `Query` is created from the `Statement` by giving the parameter
value `1`. The result of the query is then iterated, showing how to access
column values as a specific type with `at<int>()` or as a `std::string`
with `operator[]`. Both methods allow specifying either a column index or a
column name. The return value is a `std::optional` which is `std::nullopt`
if the column contains `NULL`.
```cpp
    for (Query query{select_id, {1}}; !query.empty(); query.popFront()) {
        std::cout << "row with " << query.size() << " columns:\n";
        std::cout << " id = " << query.at<int>("id").value_or(-1) << '\n';
        std::cout << " name = " << query["name"].value_or("NULL") << '\n';
    }
}
} // namespace
```

Finally, `main()` invokes both functions.
```cpp
int main()
{
    writeDb("readme.sqlite");
    readDb("readme.sqlite");
}
```

Licence
-------

This program is released under a 3-clause BSD licence, see [LICENCE][].


[IWYU]: https://include-what-you-use.org/
[LICENCE]: LICENCE
[SCONS]: https://scons.org/
[SQLITE]: https://www.sqlite.org/
