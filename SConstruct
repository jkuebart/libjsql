from SCons.Tool import FindAllTools

# Allow user to specify build mode.

vars = Variables()
vars.AddVariables(
    EnumVariable(
        "MODE",
        "The build mode.",
        "release",
        ["debug", "release"]
    ),
)

env = Environment(variables=vars)
Help(vars.GenerateHelpText(env), append=True)
if vars.UnknownVariables():
    print("Unknown command line variables: {}".format(vars.UnknownVariables()))
    Exit(64) # EX_USAGE

# Prefer clang if available (not detected as part of "default").

for tool in FindAllTools(["clang", "clangxx"], env):
    env.Tool(tool)

# Common configuration.

env.Append(CPPPATH=["include", "src"])

if env.subst("$CC") == "cl":
    env.Append(CXXFLAGS=["/EHsc", "/std:c++17"])

if env.subst("$CC") in ("clang", "gcc"):
    env.Append(CCFLAGS=["-g"], CXXFLAGS=["-std=c++17"])

if env.subst("$CC") == "clang":
    env.Append(CCFLAGS=[
        "-Werror",
        "-Weverything",
        "-Wno-padded",
        "-Wno-poison-system-directories",
        "-Wno-reserved-id-macro",
    ])

if env.subst("$CC") == "gcc":
    env.Append(CCFLAGS=["-Wall", "-pedantic"])

if env.subst("$CXX") == "clang++":
    env.Append(
        CXXFLAGS=[
            "-Wno-c++98-compat",
            "-Wno-c++98-compat-pedantic",
            "-stdlib=libc++",
        ],
        LINKFLAGS=["-stdlib=libc++"],
    )

if env.subst("$MODE") == "debug":
    if env.subst("$CC") == "cl":
        env.Append(CCFLAGS=["/MDd"])

    if env.subst("$CC") in ("clang", "gcc"):
        env.Append(CCFLAGS=["-O0"])

    if env.subst("$CC") == "clang":
        env.Append(
            CCFLAGS=["-fsanitize=address,undefined"],
            LINKFLAGS=["-fsanitize=address,undefined"],
        )

    env.Tool("compilation_db")
    env.CompilationDatabase()

if env.subst("$MODE") == "release":
    env.Append(CPPDEFINES=["NDEBUG"])

    if env.subst("$CC") == "cl":
        env.Append(CCFLAGS=["/MD"])

    if env.subst("$CC") in ("clang", "gcc"):
        env.Append(CCFLAGS=["-O"])

env.SConscript(
    dirs=["."],
    duplicate=0,
    exports=["env"],
    variant_dir=env.subst("build/$MODE"),
)
