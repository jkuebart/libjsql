#ifndef LIBJSQL_DB_H
#define LIBJSQL_DB_H

#include <memory>
#include <string>
#include <utility>

struct sqlite3;

namespace jsql
{

/// Access an SQLite3 database.
class Db
{
public:
    /// A handle owns the database connection.
    using Handle = std::unique_ptr<sqlite3, int (&)(sqlite3*)>;

    /// Constants for the constructor specifying open mode.
    struct OPEN
    {
        static const int CREATE;
        static const int READONLY;
        static const int READWRITE;
    };

    /// @param filename The file name or URI.
    /// @param flags One of the OPEN flags.
    /// @param vfs The name of the VFS to use.
    Db(char const* filename,
       int flags = OPEN::READONLY,
       char const* vfs = nullptr);

    /// @note This function relies on `sqlite_errmsg()`, so it must be
    ///       invoked immediately without an intervening SQLite call that
    ///       might clobber the error message.
    /// @param result An SQLite error code.
    /// @return The result code indiating success.
    /// @throws std::runtime_error with an appropriate message if the result
    ///         code represents an error.
    int verify(int result) const;

    /// @return The underlying database handle.
    sqlite3* get() const;

private:
    std::string const mFilename;
    std::pair<Handle, int> const mDb;
};

} // namespace jsql

#endif // LIBJSQL_DB_H
