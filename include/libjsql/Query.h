#ifndef LIBJSQL_QUERY_H
#define LIBJSQL_QUERY_H

#include <cstddef>
#include <cstdint>
#include <iosfwd>
#include <memory>
#include <optional>
#include <string>
#include <string_view>
#include <variant>
#include <vector>

struct sqlite3;
struct sqlite3_stmt;

namespace jsql
{

class Statement;

/// I provide a range of rows resulting from running an SQL statement.
///
/// The Statement is performed with the given values for its parameters.
class Query
{
public:
    using size_type = std::size_t;
    using Arguments = std::vector<
        std::variant<std::string_view, int, unsigned, std::int64_t>>;

    /// @param stmt The statement for this query.
    /// @param args Arguments to bind to the statement.
    /// @note Any `string_view`s given as arguments must be valid for the
    ///       entire lifetime of this object.
    ///
    /// @note Only *one* Query object can be created for a Statement at
    ///       any one time. Attempting to create a Query while another one is
    ///       still in scope for the same Statement will raise an exception.
    ///
    /// @throws std::runtime_error if a Query for this Statement is still in
    ///         scope.
    Query(Statement& stmt, Arguments const& args = {});

    ~Query() noexcept;

    /// @note This function relies on `sqlite_errmsg()`, so it must be
    ///       invoked immediately without an intervening SQLite call that
    ///       might clobber the error message.
    /// @param result An SQLite result code.
    /// @return The result code indiating success.
    /// @throws std::runtime_error with an appropriate message if the result
    ///         code represents an error.
    int verify(int result) const;

    /// @return Whether this query returns another row.
    bool empty() const;

    /// @return The current row.
    Query const& front() const;

    /// Move to the next row in the result.
    void popFront();

    /// @return The number of columns of the current row.
    size_type size() const;

    /// @param column A column index.
    /// @return The name assigned to the given column.
    /// @throws std::out_of_range if the column index is invalid.
    std::string columnName(size_type column) const;

    /// @param column A column name.
    /// @return The index of the named column.
    /// @throws std::out_of_range if the named column doesn't exist.
    size_type columnIndex(std::string_view const& column) const;

    /// @param column A column index.
    /// @return Whether the given column is NULL.
    /// @throws std::out_of_range if the column index is invalid.
    bool isNull(size_type column) const;

    /// @param column A column name.
    /// @return Whether the named column is NULL.
    /// @throws std::out_of_range if the named column doesn't exist.
    bool isNull(std::string_view const& column) const;

    /// @param column A column index.
    /// @return The value of the given column converted to type T, or
    ///         std::nullopt if the column is NULL.
    /// @throws std::out_of_range if the column index is invalid.
    template<typename T>
    std::optional<T> at(size_type column) const;

    /// @param column A column name.
    /// @return The value of the named column converted to type T, or
    ///         std::nullopt if the column is NULL.
    /// @throws std::out_of_range if the named column doesn't exist.
    template<typename T>
    std::optional<T> at(std::string_view const& column) const;

    /// @param column A column index.
    /// @return The value of the given column as a string_view, or
    ///         std::nullopt if the column is NULL.
    /// @throws std::out_of_range if the column index is invalid.
    std::optional<std::string> operator[](size_type column) const;

    /// @param column A column name.
    /// @return The value of the named column as a string_view, or
    ///         std::nullopt if the column is NULL.
    /// @throws std::out_of_range if the named column doesn't exist.
    std::optional<std::string> operator[](std::string_view const& column) const;

    /// @return The underlying statement handle.
    sqlite3_stmt* get() const;

    /// @return The underlying database handle.
    sqlite3* db() const;

private:
    Statement& mStmt;
    std::unique_ptr<sqlite3_stmt, int (*)(sqlite3_stmt*)> mStatement;
    bool mDone;
};

template<>
std::optional<std::string> Query::at<std::string>(size_type column) const;

template<>
std::optional<int> Query::at<int>(size_type column) const;

template<>
std::optional<unsigned> Query::at<unsigned>(size_type column) const;

template<>
std::optional<long long> Query::at<long long>(size_type column) const;

template<typename T>
std::optional<T> Query::at(std::string_view const& column) const
{
    return at<T>(columnIndex(column));
}

std::ostream& operator<<(std::ostream& os, Query const& query);

} // namespace jsql

#endif // LIBJSQL_QUERY_H
