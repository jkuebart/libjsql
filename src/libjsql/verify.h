#ifndef LIBJSQL_VERIFY_H
#define LIBJSQL_VERIFY_H

#include <string_view>

namespace jsql
{

/// @param result An SQLite error code.
/// @param topic An optional additional error message.
/// @param detail An optional additional error message.
/// @return The result code indiating success.
/// @throws std::runtime_error with an appropriate message if the result
///         code represents an error.
int verify(
    int result,
    std::string_view const& topic = "",
    std::string_view const& detail = "");

} // namespace jsql

#endif // LIBJSQL_VERIFY_H
