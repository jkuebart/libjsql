#include "libjsql/Query.h"

#include "libjsql/Statement.h"
#include "libjsql/verify.h"

#include <sqlite3.h>

#include <iomanip>
#include <ostream>
#include <stdexcept>
#include <type_traits>
#include <unordered_map>

namespace
{

template<typename... Ts>
struct overloaded : Ts...
{
    using Ts::operator()...;
};

template<typename... Ts>
overloaded(Ts...) -> overloaded<Ts...>;

} // namespace

namespace jsql
{

Query::Query(Statement& stmt, Arguments const& args)
: mStmt{stmt}
, mStatement{std::move(stmt.mStatement)}
, mDone{false}
{
    if (!mStatement) {
        throw std::runtime_error{
            "Can't create a Query while another one is still in scope."};
    }

    for (unsigned index{0}; index != args.size(); ++index) {
        int const idx{1 + static_cast<int>(index)};
        std::visit(
            overloaded{
                [this, idx](std::string_view const& s) {
                    verify(sqlite3_bind_text(
                        mStatement.get(),
                        idx,
                        s.data(),
                        static_cast<int>(s.size()),
                        nullptr));
                },
                [this, idx](int const i) {
                    verify(sqlite3_bind_int(mStatement.get(), idx, i));
                },
                [this, idx](unsigned const u) {
                    verify(sqlite3_bind_int64(
                        mStatement.get(),
                        idx,
                        static_cast<sqlite_int64>(u)));
                },
                [this, idx](std::int64_t const i) {
                    verify(sqlite3_bind_int64(mStatement.get(), idx, i));
                },
            },
            args[index]);
    }
    popFront();
}

Query::~Query() noexcept
{
    sqlite3_clear_bindings(mStatement.get());
    sqlite3_reset(mStatement.get());
    mStmt.mStatement = std::move(mStatement);
}

int Query::verify(int const result) const
{
    return ::jsql::verify(result, "", sqlite3_errmsg(db()));
}

bool Query::empty() const
{
    return mDone;
}

Query const& Query::front() const
{
    return *this;
}

void Query::popFront()
{
    mDone = (SQLITE_DONE == verify(sqlite3_step(mStatement.get())));
}

Statement::size_type Query::size() const
{
    return mStmt.mSize;
}

std::string Query::columnName(size_type const column) const
{
    return sqlite3_column_name(mStatement.get(), mStmt.checkColumn(column));
}

Statement::size_type Query::columnIndex(std::string_view const& column) const
{
    if (mStmt.mColumnNames.empty()) {
        for (unsigned clmn{0}; clmn != mStmt.mSize; ++clmn) {
            mStmt.mColumnNames.emplace(columnName(clmn), clmn);
        }
    }
    return mStmt.mColumnNames.at(std::string{column});
}

bool Query::isNull(size_type const column) const
{
    return SQLITE_NULL ==
        sqlite3_column_type(mStatement.get(), mStmt.checkColumn(column));
}

bool Query::isNull(std::string_view const& column) const
{
    return isNull(columnIndex(column));
}

template<>
std::optional<std::string> Query::at<std::string>(size_type const column) const
{
    // We don't require a terminating NUL, so use _blob instead of _text.
    if (void const* const data{
            sqlite3_column_blob(mStatement.get(), mStmt.checkColumn(column))})
    {
        return std::string{
            static_cast<char const*>(data),
            static_cast<std::string::size_type>(sqlite3_column_bytes(
                mStatement.get(),
                mStmt.checkColumn(column)))};
    }

    // NULL could indicate an error.
    verify(sqlite3_errcode(db()));

    // NULL could indicate length zero.
    if (!isNull(column)) {
        return "";
    }

    // NULL could indicate a NULL value.
    return std::nullopt;
}

template<>
std::optional<int> Query::at<int>(size_type const column) const
{
    if (!isNull(column)) {
        return sqlite3_column_int(mStatement.get(), mStmt.checkColumn(column));
    }
    return std::nullopt;
}

template<>
std::optional<unsigned> Query::at<unsigned>(size_type const column) const
{
    if (auto const result{at<int>(column)}) {
        if (*result < 0) {
            throw std::range_error{
                "Result " + std::to_string(*result) + " is negative."};
        }
        return static_cast<unsigned>(*result);
    }
    return std::nullopt;
}

template<>
std::optional<long long> Query::at<long long>(size_type const column) const
{
    if (!isNull(column)) {
        return sqlite3_column_int64(
            mStatement.get(),
            mStmt.checkColumn(column));
    }
    return std::nullopt;
}

std::optional<std::string> Query::operator[](size_type const column) const
{
    return at<std::string>(column);
}

std::optional<std::string> Query::operator[](
    std::string_view const& column) const
{
    return at<std::string>(column);
}

sqlite3_stmt* Query::get() const
{
    return mStatement.get();
}

sqlite3* Query::db() const
{
    return sqlite3_db_handle(mStatement.get());
}

std::ostream& operator<<(std::ostream& os, Query const& query)
{
    os << '{';
    char const* sep{""};
    if (!query.empty()) {
        for (Statement::size_type column{0}; column != query.size(); ++column) {
            os << sep << query.columnName(column) << '='
               << std::quoted(query[column].value_or("NULL"));
            sep = " ";
        }
    }
    return os << '}';
}

} // namespace jsql
