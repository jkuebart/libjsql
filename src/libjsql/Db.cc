#include "libjsql/Db.h"

#include "libjsql/verify.h"

#include <sqlite3.h>

namespace jsql
{

namespace
{

    std::pair<Db::Handle, int> openDatabase(
        char const* const filename,
        int const flags,
        char const* const vfs)
    {
        sqlite3* db;
        int const result{sqlite3_open_v2(filename, &db, flags, vfs)};

        // Using sqlite3_close_v2 ensures that subsequent uses of this
        // handle raise an exception. Conversely, sqlite3_close would
        // return SQLITE_BUSY which is ignored when used as a deleter.
        return {Db::Handle{db, sqlite3_close_v2}, result};
    }

} // namespace

const int Db::OPEN::CREATE{SQLITE_OPEN_CREATE};
const int Db::OPEN::READONLY{SQLITE_OPEN_READONLY};
const int Db::OPEN::READWRITE{SQLITE_OPEN_READWRITE};

Db::Db(char const* const filename, int const flags, char const* const vfs)
: mFilename{filename}
, mDb{openDatabase(filename, flags, vfs)}
{
    verify(mDb.second);
}

int Db::verify(int const result) const
{
    return ::jsql::verify(result, mFilename, sqlite3_errmsg(get()));
}

sqlite3* Db::get() const
{
    return mDb.first.get();
}

} // namespace jsql
